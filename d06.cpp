#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

std::vector<long> parse(const std::string& filename)
{
	std::ifstream infile(filename);
	std::string el;
	std::vector<int> vec;
	while (std::getline(infile, el, ',')) {
		vec.push_back(std::stoi(el));
	}
	std::vector<long> out(9);
	for (int i { 0 }; i < 9; ++i) {
		out[i] = std::ranges::count(vec, i);
	}
	return out;
}

long growth(std::vector<long> popn, int days)
{
	for (int d { 0 }; d < days; ++d) {
		long tmp { popn[0] };
		for (int i { 0 }; i < 8; ++i) {
			popn[i] = popn[i + 1];
		}
		popn[6] += tmp;
		popn[8] = tmp;
	}
	return std::reduce(popn.begin(), popn.end());
}

int main()
{
	std::vector<long> t { parse("i06t0.txt") };
	std::vector<long> d { parse("i06.txt") };

	std::cout << (growth(t, 80) == 5934) << '\n'
						<< (growth(d, 80) == 380612) << '\n'
						<< (growth(t, 256) == 26984457539) << '\n'
						<< (growth(d, 256) == 1710166656900) << '\n';
}
