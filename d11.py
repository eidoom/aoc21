#!/usr/bin/env python3

from copy import deepcopy


class Grid:
    def __init__(self, string):
        self.width = string.find("\n")
        self.data = [int(i) for i in string.replace("\n", "")]
        self.height = len(self.data) // self.width
        self.flashes = 0
        self.reset_flashed()

    def reset_flashed(self):
        self.flashed = [[False] * self.width for _ in range(self.height)]

    def get(self, i, j):
        return self.data[i * self.width + j]

    def set(self, i, j, v):
        self.data[i * self.width + j] = v

    def inc(self, i, j, v=1):
        self.data[i * self.width + j] += v

    def charged(self, i, j):
        return self.get(i, j) > 9

    @staticmethod
    def left(i):
        return i > 0

    def right(self, i):
        return i + 1 < self.height

    @staticmethod
    def up(j):
        return j > 0

    def down(self, j):
        return j + 1 < self.width

    def energise(self, i, j):
        self.inc(i, j)

        if self.charged(i, j) and not self.flashed[i][j]:
            self.flashed[i][j] = True
            self.flashes += 1

            if self.left(i):
                self.energise(i - 1, j)

                if self.up(j):
                    self.energise(i - 1, j - 1)

            if self.down(j):
                self.energise(i, j + 1)

                if self.left(i):
                    self.energise(i - 1, j + 1)

            if self.right(i):
                self.energise(i + 1, j)

                if self.down(j):
                    self.energise(i + 1, j + 1)

            if self.up(j):
                self.energise(i, j - 1)

                if self.right(i):
                    self.energise(i + 1, j - 1)

    def step(self):
        for i in range(self.height):
            for j in range(self.width):
                self.energise(i, j)

        for i in range(self.height):
            for j in range(self.width):
                if self.charged(i, j):
                    self.set(i, j, 0)

        self.reset_flashed()

    def string(self):
        return "".join(
            "".join((str(c) for c in self.data[self.width * i : self.width * (i + 1)]))
            + ("\n" if i != self.width - 1 else "")
            for i in range(self.width)
        )


def parse(filename):
    with open(filename, "r") as f:
        data = f.read().strip()
    return Grid(data)


def iterate(grid, steps):
    for _ in range(steps):
        grid.step()
    return grid


def sync(grid):
    i = 0
    while True:
        i += 1
        grid.step()
        if not any(grid.data):
            return i


if __name__ == "__main__":
    quick = parse("i11t0.txt")
    test = parse("i11t1.txt")
    data = parse("i11.txt")

    print(
        "[test 0: step 0]",
        iterate(deepcopy(quick), 0).string() == "11111\n19991\n19191\n19991\n11111",
    )
    print(
        "[test 0: step 1]",
        iterate(deepcopy(quick), 1).string() == "34543\n40004\n50005\n40004\n34543",
    )
    print(
        "[test 0: step 2]",
        iterate(deepcopy(quick), 2).string() == "45654\n51115\n61116\n51115\n45654",
    )

    print(
        "[test 1: step 0]",
        iterate(deepcopy(test), 0).string()
        == "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526",
    )
    print(
        "[test 1: step 1]",
        iterate(deepcopy(test), 1).string()
        == "6594254334\n3856965822\n6375667284\n7252447257\n7468496589\n5278635756\n3287952832\n7993992245\n5957959665\n6394862637",
    )
    print(
        "[test 1: step 2]",
        iterate(deepcopy(test), 2).string()
        == "8807476555\n5089087054\n8597889608\n8485769600\n8700908800\n6600088989\n6800005943\n0000007456\n9000000876\n8700006848",
    )

    print(ta0 := iterate(deepcopy(test), 10).flashes, ta0 == 204)
    print(ta1 := iterate(deepcopy(test), 100).flashes, ta1 == 1656)
    print(ra := iterate(deepcopy(data), 100).flashes, ra == 1632)

    print(tb := sync(test), tb == 195)
    print(rb := sync(data), rb == 303)
