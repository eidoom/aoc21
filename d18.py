#!/usr/bin/env python3

import math
import functools
import itertools


def proc(pairs_str):
    return [
        int(x) if x not in ("[", "]") else x for x in pairs_str if x not in (",", " ")
    ]


def parse(filename):
    with open(filename, "r") as f:
        return [proc(sfn) for sfn in f.read().strip("\n").splitlines()]


def explode(pairs):
    count_open = 0
    for i, v in enumerate(pairs):
        if v == "[":
            count_open += 1
        elif v == "]":
            count_open -= 1

        if count_open == 5:
            ii = i - 1
            while ii != 0:
                if isinstance(pairs[ii], int):
                    pairs[ii] += pairs[i + 1]
                    break
                ii -= 1
            ii = i + 4
            while ii < len(pairs):
                if isinstance(pairs[ii], int):
                    pairs[ii] += pairs[i + 2]
                    break
                ii += 1
            return pairs[:i] + [0] + pairs[i + 4 :]


def split(pairs):
    for i, v in enumerate(pairs):
        if isinstance(v, int) and v > 9:
            return pairs[:i] + ["[", v // 2, math.ceil(v / 2), "]"] + pairs[i + 1 :]


def add(a, b):
    c = ["["] + a + b + ["]"]
    while True:
        d = explode(c)
        if d is not None:
            c = d
            continue
        e = split(c)
        if e is not None:
            c = e
            continue
        if d is None and e is None:
            return c


def add_all(sfns):
    return functools.reduce(add, sfns)


def stringify(lst):
    s = ""
    for i, e in enumerate(lst):
        if isinstance(e, int):
            if isinstance(lst[i - 1], int) or lst[i - 1] == "]":
                s += ","
            s += str(e)
        else:
            if (
                e == "["
                and i > 0
                and (lst[i - 1] == "]" or isinstance(lst[i - 1], int))
            ):
                s += ","
            s += e
    return s


def magnitude(a, b):
    if isinstance(a, list):
        a = magnitude(*a)
    if isinstance(b, list):
        b = magnitude(*b)
    return 3 * a + 2 * b


def score(sfns):
    return magnitude(*eval(stringify(add_all(sfns))))


def score2(sfns):
    return max(score(x) for x in itertools.permutations(sfns, 2))


if __name__ == "__main__":
    # print(stringify(explode(proc("[[[[[9, 8], 1], 2], 3], 4]"))))
    # print(stringify(explode(proc("[7, [6, [5, [4, [3, 2]]]]]"))))
    # print(stringify(explode(proc("[[6, [5, [4, [3, 2]]]], 1]"))))
    # print(stringify(explode(proc("[[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]]"))))
    # print(stringify(explode(proc("[[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]]"))))
    # print(stringify(split(["[", 5, 11, 10, 1, 12, "]"])))

    print(m1 := magnitude(*[9, 1]), m1 == 29)
    print(m2 := magnitude(*[1, 9]), m2 == 21)
    print(m3 := magnitude(*[[9, 1], [1, 9]]), m3 == 129)
    print(m4 := magnitude(*[[1, 2], [[3, 4], 5]]), m4 == 143)
    print(m4 := magnitude(*[[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]]), m4 == 1384)

    t0 = parse("i18t0.txt")
    t1 = parse("i18t1.txt")
    t2 = parse("i18t2.txt")
    data = parse("i18.txt")

    print(ta1 := stringify(add_all(t1)), ta1 == "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")
    print(ta2 := stringify(add_all(t2[:4])), ta2 == "[[[[1,1],[2,2]],[3,3]],[4,4]]")
    print(ta3 := stringify(add_all(t2[:5])), ta3 == "[[[[3,0],[5,3]],[4,4]],[5,5]]")
    print(ta4 := stringify(add_all(t2[:6])), ta4 == "[[[[5,0],[7,4]],[5,5]],[6,6]]")

    print(ta := score(t0), ta == 4140)
    print(ra := score(data), ra == 4120)

    print(tb := score2(t0), tb == 3993)
    print(rb := score2(data), rb == 4725)
