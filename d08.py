#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        return [
            [b.split() for b in a.split(" | ")]
            for a in f.read().strip("\n").splitlines()
        ]


# D: #
# 0: 6
# 1: 2
# 2: 5
# 3: 5
# 4: 4
# 5: 5
# 6: 6
# 7: 3
# 8: 7
# 9: 6

# #: D
# 2: 1
# 3: 7
# 4: 4
# 5: 2,3,5
# 6: 0,6,9
# 7: 8


def interp_row(patterns, output):
    known = [set(p) for p in patterns if len(p) in (2, 3, 4, 7)]
    return sum([set(o) in known for o in output])


# that's ugly...
def interp_row_full(patterns):
    n = [None] * 10
    c235 = []
    # c069 = []
    for p in patterns:
        match len(p):
            case 2:
                n[1] = set(p)
            case 3:
                n[7] = set(p)
            case 4:
                n[4] = set(p)
            case 7:
                n[8] = set(p)
            case 5:
                c235.append(set(p))
            # case 6:
            #     c069.append(set(p))
    a = n[1] ^ n[7] # symmetric difference
    b_d = n[4] ^ n[1]
    e_g = n[8] ^ n[4] ^ a
    c25 = []
    for x in c235:
        if x > n[1]: # superset
            n[3] = x
        else:
            c25.append(x)
    b_g = n[3] ^ n[4] ^ a
    b = b_d & b_g # intersection
    g = e_g & b_g
    d = b_d ^ b
    e = e_g ^ g
    for x in c25:
        if b & x:
            n[5] = x
        else:
            n[2] = x
    c = n[2] ^ a ^ d ^ e ^ g
    f = n[1] ^ c
    n[0] = n[8] - d # difference
    n[6] = n[8] - c
    n[9] = n[8] - e
    return n

def decode(keys, output):
    # t = 0
    t = ""
    for code in output:
        # for e, code in enumerate(output):
        for i, key in enumerate(keys):
            if set(code) == key:
                # t += i * 10 ** (3 - e)
                t += str(i)
                break
    # return t
    return int(t)

def interp(notes):
    return sum(interp_row(*row) for row in notes)

def interp_full(notes):
    return sum(decode(interp_row_full(patterns), outputs) for patterns, outputs in notes)

if __name__ == "__main__":
    test0 = parse("i08t0.txt")
    test = parse("i08t1.txt")
    data = parse("i08.txt")

    print(tz := interp(test0), tz == 0)
    print(ta := interp(test), ta == 26)
    print(ra := interp(data), ra == 261)

    print(tbz := interp_full(test0), tbz == 5353)
    print(tb := interp_full(test), tb == 61229)
    print(rb := interp_full(data), rb == 987553)
