#!/usr/bin/env python3

from collections import defaultdict


def build_graph(pairs):
    graph = defaultdict(list)
    for a, b in pairs:
        graph[a].append(b)
        graph[b].append(a)
    return graph


def parse(filename):
    with open(filename, "r") as f:
        return build_graph([a.split("-") for a in f.read().strip("\n").splitlines()])


def all_paths(graph, repeats):
    paths = [["start"]]
    complete = 0

    while len(paths):
        prev = paths.pop()
        repeated = any(
            prev.count(x) == repeats for x in prev if x.islower() and x != "start"
        )

        for branch in graph[prev[-1]]:
            if branch == "start" or (branch.islower() and repeated and branch in prev):
                continue

            if branch == "end":
                complete += 1
            else:
                paths.append(prev + [branch])

    return complete


if __name__ == "__main__":
    test0 = parse("i12t0.txt")
    test1 = parse("i12t1.txt")
    test2 = parse("i12t2.txt")
    data = parse("i12.txt")

    print(ta0 := all_paths(test0, 1), ta0 == 10)
    print(ta1 := all_paths(test1, 1), ta1 == 19)
    print(ta2 := all_paths(test2, 1), ta2 == 226)
    print(ra := all_paths(data, 1), ra == 3856)

    print(tb0 := all_paths(test0, 2), tb0 == 36)
    print(tb1 := all_paths(test1, 2), tb1 == 103)
    print(tb2 := all_paths(test2, 2), tb2 == 3509)
    print(rb := all_paths(data, 2), rb == 116692)
