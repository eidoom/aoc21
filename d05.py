#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        return [
            [[int(x) for x in pair.split(",")] for pair in line.split(" -> ")]
            for line in f.read().strip("\n").splitlines()
        ]


def add_dedup(line_pts, overlap_pts, new):
    if new in line_pts:
        overlap_pts.add(new)
    else:
        line_pts.add(new)


def auto_range(x0, x1):
    m = 1 if x1 > x0 else -1
    return range(x0, x1 + m, m)


def count_overlaps(data, p2=False):
    points = set()
    overlaps = set()
    for ((i0, j0), (i1, j1)) in data:
        if i0 == i1:
            for j in auto_range(j0, j1):
                add_dedup(points, overlaps, (i0, j))
        elif j0 == j1:
            for i in auto_range(i0, i1):
                add_dedup(points, overlaps, (i, j0))
        elif p2:
            for i, j in zip(auto_range(i0, i1), auto_range(j0, j1)):
                add_dedup(points, overlaps, (i, j))
    return len(overlaps)


if __name__ == "__main__":
    test = parse("i05t0.txt")
    data = parse("i05.txt")

    print(ta := count_overlaps(test), ta == 5)
    print(ra := count_overlaps(data), ra == 8111)

    print(tb := count_overlaps(test, True), tb == 12)
    print(rb := count_overlaps(data, True), rb == 22088)
