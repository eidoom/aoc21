#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        data = f.read().strip("\n").split("\n\n")
    return [int(x) for x in data[0].split(",")], [
        [[[int(z), False] for z in y.split()] for y in x.splitlines()] for x in data[1:]
    ]


def win_line(line):
    return all(b for _, b in line)


def win(board):
    for i in range(len(board[0])):
        if win_line(board[i]) or win_line(row[i] for row in board):
            return True


def bingo(draw, boards, second=False):
    for num in draw:
        for i, board in enumerate(boards):
            for j, row in enumerate(board):
                for k, (e, _) in enumerate(row):
                    if num == e:
                        boards[i][j][k][1] = True
            if win(board):
                return (
                    num * sum(v for v, b in (e for row in board for e in row) if not b)
                    if not second or len(boards) == 1
                    else bingo(draw, boards[:i] + boards[i + 1 :], second)
                )


if __name__ == "__main__":
    test = parse("i04t0.txt")
    data = parse("i04.txt")

    print(ta := bingo(*test), ta == 4512)
    print(ra := bingo(*data), ra == 10680)

    print(tb := bingo(*test, True), tb == 1924)
    print(rb := bingo(*data, True), rb == 31892)
