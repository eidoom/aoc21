#!/usr/bin/env -S sbcl --script

; unfinished

(require "asdf")

(defun split-up (str)
	(let ((a (position #\, str)))
		(let ((x0 (subseq str 0 a))
					(b (+ 1 a (position #\space  (subseq str (+ a 1))))))
			(let ((y0 (subseq str (+ a 1) b))
						(c (+ b 3)))
				(let ((d (+ 1 c (position #\, (subseq str (+ c 1))))))
					(let ((x1 (subseq str (+ c 1) d))
								(y1 (subseq str (+ d 1))))
						(mapcar #'parse-integer (list x0 y0 x1 y1))
						))))))

(defun read-file (filename)
	(mapcar #'split-up (uiop:read-file-lines filename)))

(defun count-overlaps (data)
	(let ((points ())
				(overlaps ()))
		(dolist (coords data)
			(let ((i0 (first coords))
						(j0 (second coords))
						(i1 (third coords))
						(j1 (fourth coords)))
				(cond ((= i0 i1) ())
							((= j0 j1) ())
							)))))

(time
	(let ((t0 (read-file "i05t0.txt"))
				(i (read-file "i05.txt")))

		(print (count-overlaps t0))

		; (format t "~a~%" (= 4512 (bingo t0)))
		; (format t "~a~%" (= 10680 (bingo i)))

		; (format t "~a~%" (= 1924 (lose t0)))
		; (format t "~a~%" (= 31892 (lose i)))
		))
