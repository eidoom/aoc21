#!/usr/bin/env python3

from copy import deepcopy


def parse(filename):
    with open(filename, "r") as f:
        fishes = [int(x) for x in f.read().strip("\n").split(",")]
    return [fishes.count(x) for x in range(9)]


def growth(popn, days):
    for _ in range(days):
        tmp = popn[0]
        for i in range(8):
            popn[i] = popn[i + 1]
        popn[6] += tmp
        popn[8] = tmp
    return sum(popn)


if __name__ == "__main__":
    test = parse("i06t0.txt")
    data = parse("i06.txt")

    da = 80
    print(ta := growth(deepcopy(test), da), ta == 5934)
    print(ra := growth(deepcopy(data), da), ra == 380612)

    db = 256
    print(tb := growth(deepcopy(test), db), tb == 26984457539)
    print(rb := growth(deepcopy(data), db), rb == 1710166656900)
