#!/usr/bin/env python3

# https://www.reddit.com/r/adventofcode/comments/rnejv5/2021_day_24_solutions/?sort=top
# https://www.reddit.com/r/adventofcode/comments/rnejv5/comment/hps5hgw/?utm_source=share&utm_medium=web2x&context=3
# https://github.com/dphilipson/advent-of-code-2021/blob/master/src/days/day24.rs

import re


def parse(filename):
    with open(filename, "r") as f:
        return [line.split(" ") for line in f.read().strip("\n").splitlines()]


def alu(instructions, inputs):
    inputs = (int(x) for x in str(inputs))
    var = {"w": 0, "x": 0, "y": 0, "z": 0}
    for inst in instructions:
        if inst[0] == "inp":
            v = next(inputs)
            if not (1 <= v <= 9):
                raise Exception("input not in 1..9")
            var[inst[1]] = v
        else:
            op, a, b = inst
            b = var[b] if b in ("w", "x", "y", "z") else int(b)
            match op:
                case "add":
                    var[a] += b
                case "mul":
                    var[a] *= b
                case "div":
                    var[a] //= b
                case "mod":
                    var[a] %= b
                case "eql":
                    var[a] = 1 if var[a] == b else 0
    return var


# monad is just the following repeated 14 times:
"""
inp w
mul x 0
add x z
mod x 26
div z {a}
add x {b}
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y {c}
mul y x
add z y
"""
# this condenses to
"""
def link(inp, z, a, b, c):
    x = int(z % 26 + b != inp)
    return x * (inp + c) + z // a * (25 * x + 1)
"""
# but a in {1,26}, with 7 of each
# when a=1,
# i in {0, 1, 2, 4, 6, 7, 8} (ith input digits)
# b>=11 (run `variables("i24.txt")` to see)
# so `x = int(z % 26 + b != inp)` is 1 (since inp in [1,9] and `z % 26 + b`>=11)
# and `z // a` is z
# so we get
"""
z = (inp + c) + z * 26
"""
# creating an output number z in base 26 by appending (inp + c) and shifting the existing digits_26 up by one - "push"
# now, we want final output zero, so we need to subtract these digits using `z //= (a=26)` with the other 7 operations - "pop"
# this brings us to the a=26 cases
# these are on i in {3, 5, 9, 10, 11, 12, 13}
# and b<=0
# for pop, we want `z = z // a` => `x * (inp + c) + z // a * (25 * x + 1) == z // a`
# => `x == 0` => `z % 26 + b == inp` => `inp == (inp_prev + c_prev) % 26 + b`
# so: `inp[i] = inp[i-1] + c[i-1] + b[i]`
# except the [i] index is on the stack of pushed numbers:
# `build_constraints()` ->
# i[3]  = i[2] + 5  - 11  (-6)
# i[5]  = i[4] + 5  + 0   (+5)
# i[9]  = i[8] + 1  - 6   (-5)
# i[10] = i[7] + 11 - 10  (+1)
# i[11] = i[6] + 4  - 12  (-8)
# i[12] = i[1] + 10 - 3   (+7)
# i[13] = i[0] + 13 - 5   (+8)
# then we just solve the pop inputs to give the lowest/highest model number


def variables(filename):
    with open(filename, "r") as f:
        monad = f.read().strip("\n")
    regex = r"inp w\nmul x 0\nadd x z\nmod x 26\ndiv z (-?\d+)\nadd x (-?\d+)\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y (\d+)\nmul y x\nadd z y"
    return [list(map(int, x)) for x in re.findall(regex, monad)]


def build_constraints():
    var = variables("i24.txt")
    pushes = []
    for i, (a, b, c) in enumerate(var):
        if a == 1:
            pushes.append(i)
        elif a == 26:
            j = pushes.pop()
            print(f"inp[{i}] = inp[{j}] + {var[j][2] + b}")


def chain(var, num):
    z = 0
    for (a, b, c), inp in zip(var, [int(c) for c in str(num)]):
        x = int(z % 26 + b != inp)
        z = x * (inp + c) + z // a * (25 * x + 1)
    return z


def build_biggest():
    i = [9] * 14

    i[0] = 1
    i[1] = 2
    i[4] = 4
    i[7] = 8

    i[3] = i[2] - 6
    i[5] = i[4] + 5
    i[9] = i[8] - 5
    i[10] = i[7] + 1
    i[11] = i[6] - 8
    i[12] = i[1] + 7
    i[13] = i[0] + 8

    return "".join(str(j) for j in i)


def build_smallest():
    i = [1] * 14

    i[2] = 7
    i[8] = 6
    i[6] = 9

    i[3] = i[2] - 6
    i[5] = i[4] + 5
    i[9] = i[8] - 5
    i[10] = i[7] + 1
    i[11] = i[6] - 8
    i[12] = i[1] + 7
    i[13] = i[0] + 8

    return "".join(str(j) for j in i)


if __name__ == "__main__":
    t0 = parse("i24t0.txt")
    t1 = parse("i24t1.txt")
    t2 = parse("i24t2.txt")

    print(alu(t0, 1)["x"] == -1)
    print(alu(t1, 13)["z"] == 1)
    print(alu(t1, 41)["z"] == 0)
    print("".join(map(str, alu(t2, 9).values())) == "1001")

    monad_file = "i24.txt"
    monad = parse(monad_file)

    var = variables(monad_file)
    print(var)

    t3 = 13579246899999
    print(alu(monad, t3)["z"] == chain(var, t3))

    build_constraints()

    biggest = build_biggest()
    print(biggest)
    print(chain(var, biggest) == 0)

    smallest = build_smallest()
    print(smallest)
    print(chain(var, smallest) == 0)
