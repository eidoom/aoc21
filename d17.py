#!/usr/bin/env python3

import re


def parse(filename):
    with open(filename, "r") as f:
        return [
            int(i)
            for i in re.match(
                r"target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)..(-?\d+)",
                f.readline().strip("\n"),
            ).groups()
        ]


def shoot(j0, j1, i0, i1, vi, vj, i=0, j=0):
    max_i = 0

    if i0 > i1:
        i0, i1 = i1, i0
    if j0 > j1:
        j0, j1 = j1, j0

    while True:
        i += vi
        j += vj

        if i > max_i:
            max_i = i

        if i0 <= i <= i1 and j0 <= j <= j1:
            return max_i

        # assumes target in lower quadrant
        if i < i0:
            return None

        vi -= 1
        if vj < 0:
            vj += 1
        elif vj > 0:
            vj -= 1


def shoot2(j0, j1, i0, i1, vi, vj, i=0, j=0):
    if i0 > i1:
        i0, i1 = i1, i0
    if j0 > j1:
        j0, j1 = j1, j0

    while True:
        i += vi
        j += vj

        if i0 <= i <= i1 and j0 <= j <= j1:
            return True

        # assumes target in lower quadrant
        if i < i0:
            return False

        vi -= 1
        if vj < 0:
            vj += 1
        elif vj > 0:
            vj -= 1


def optim(target):
    max_v = 100
    max_i = 0
    for vi in range(-max_v, max_v):
        for vj in range(-max_v, max_v):
            if ii := shoot(*target, vi, vj):
                if ii > max_i:
                    max_i = ii
    return max_i


def every(target, max_v):
    vs = range(-max_v, max_v)
    return sum(1 for vi in vs for vj in vs if shoot2(*target, vi, vj))


if __name__ == "__main__":
    t0 = parse("i17t0.txt")
    data = parse("i17.txt")

    print(ta := optim(t0), ta == 45)
    print(ra := optim(data), ra == 3655)

    print(tb := every(t0, 100), tb == 112)
    print(rb := every(data, 300), rb == 1447)
