#!/usr/bin/env python3

import re
import collections
import operator


def parse(filename):
    with open(filename, "r") as f:
        return [
            (*map(int, re.findall(r"(-?\d+)", x)), x[1] == "n")
            for x in f.read().strip("\n").splitlines()
        ]


def count(cuboids):
    return sum(
        w * (x1 - x0 + 1) * (y1 - y0 + 1) * (z1 - z0 + 1)
        for (x0, x1, y0, y1, z0, z1), w in cuboids.items()
    )


# https://en.wikipedia.org/wiki/Inclusion-exclusion_principle
def reboot(steps, lim):
    cuboids = collections.Counter()

    for x0, x1, y0, y1, z0, z1, on in steps:
        if all(q0 >= -lim and q1 <= lim for q0, q1 in ((x0, x1), (y0, y1), (z0, z1))):
            intersections = collections.Counter()

            for (x2, x3, y2, y3, z2, z3), weight in cuboids.items():
                """
                to subtract coordinates that would otherwise be multiply counted by existing cuboids (new positive cuboid switched on at end),
                if on and weight > 0:
                    create intersection cuboid with -abs(weight) to zero subcuboid of existing positive cuboid
                elif on and weight < 0:
                    create intersection cuboid with +abs(weight) to zero subcuboid of existing negative cuboid
                elif not on and weight > 0:
                    create intersection cuboid with -abs(weight) to switch off subcuboid of existing positive cuboid
                elif not on and weight < 0:
                    create intersection cuboid with +abs(weight) since first negative cuboid was created to, e.g., switch off an existing positive subcuboid (previous condition), and this new negative cuboid will switch off a subvolume of that same positive subcuboid, so we need a new positive intersection cuboid of the two negative cuboids to stop multiple counting
                """
                # `a if a > b else b` is more performant than `max`, and similarly for `min`, giving 1/3 runtime, but less readable
                xa, ya, za = map(max, (x0, y0, z0), (x2, y2, z2))
                xb, yb, zb = map(min, (x1, y1, z1), (x3, y3, z3))

                if all(map(operator.le, (xa, ya, za), (xb, yb, zb))):
                    intersections[(xa, xb, ya, yb, za, zb)] -= weight

            cuboids.update(intersections)

            if on:
                cuboids[(x0, x1, y0, y1, z0, z1)] += 1

            # deleting zero weighted cuboids halves runtime
            for coords in [c for c, w in cuboids.items() if not w]:
                del cuboids[coords]

    return count(cuboids)


if __name__ == "__main__":
    t0 = parse("i22t0.txt")
    t1 = parse("i22t1.txt")
    t2 = parse("i22t2.txt")
    data = parse("i22.txt")

    print(ta1 := reboot(t0, 50), ta1 == 39)
    print(ta2 := reboot(t1, 50), ta2 == 590784)
    print(ta3 := reboot(t2, 50), ta3 == 474140)
    print(ra := reboot(data, 50), ra == 610196)

    print(tb := reboot(t2, float("inf")), tb == 2758514936282235)
    print(rb := reboot(data, float("inf")), rb == 1282401587270826)
