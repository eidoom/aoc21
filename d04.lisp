#!/usr/bin/env -S sbcl --script

(require "asdf")

; how to dedup these 3 functions?

(defun split-on-subseq (str delim)
	(let ((a (search delim str)))
		(if (null a)
			(list str)
			(let ((b (length delim)))
				(cons (subseq str 0 a) (split-on-subseq (subseq str (+ a b)) delim))))))

(defun split-on-single (str delim)
	(let ((a (position delim str)))
		(if (null a)
			(list str)
			(let ((b 1))
				(cons (subseq str 0 a) (split-on-single (subseq str (+ a b)) delim))))))

; split on a single character, but it can be repeated any number of times in the delimiter
(defun split-on-block (str delim)
	(let ((a (position delim str)))
		(if (null a)
			(list str)
			(let ((b 1))
				(loop while (string= delim (elt str (+ a b))) do (incf b))
				(cons (subseq str 0 a) (split-on-block (subseq str (+ a b)) delim))))))

(defun split-on-spaces (str)
	(split-on-block (string-trim " " str) #\space))

(defun parse-board-row (str)
	(mapcar (lambda (x) (list (parse-integer x) nil)) (split-on-spaces str)))

(defun parse-board (str)
	(mapcar 'parse-board-row (split-on-single str #\newline)))

(defun read-file (filename)
	(let ((a (split-on-subseq (string-trim '(#\newline) (uiop:read-file-string filename)) '(#\newline #\newline))))
		(list (mapcar 'parse-integer (split-on-single (first a) #\,))
					(mapcar 'parse-board (rest a)))))

(defun col (board j)
	(mapcar (lambda (row) (elt row j)) board))

(defun win-line (line)
	(= (length line) (count-if 'second line)))

; how to DRY?
(defun win (board)
	(dotimes (i (length (first board)))
		(when (or (win-line (elt board i)) (win-line (col board i))) (return-from win t))))

(defun cond-count (a b)
	(if (second b) a (+ a (first b))))

(defun score (board)
	(reduce 'cond-count (apply 'concatenate 'list board) :initial-value 0))

(defun bingo (data)
	(dolist (num (first data))
		(dolist (board (second data))
			(dolist (row board)
				(dolist (x row)
					(when (= num (first x)) (setf (second x) t))))
			(when (win board) (return-from bingo (* num (score board)))))))

(defun lose (data)
	(let ((draw (first data))
				(pairs (second data)))
		(dolist (num draw)
			(dolist (board pairs)
				(dolist (row board)
					(dolist (x row)
						(when (= num (first x)) (setf (second x) t))))
				(let ((won (win board)))
					(when (and (= 1 (length pairs)) won) (return-from lose (* num (score board))))
					(when won (return-from lose (lose (list draw (remove board pairs))))))
				))))

(time
	(let ((t0 (read-file "i04t0.txt"))
				(i (read-file "i04.txt")))

		(format t "~a~%" (= 4512 (bingo t0)))
		(format t "~a~%" (= 10680 (bingo i)))

		(format t "~a~%" (= 1924 (lose t0)))
		(format t "~a~%" (= 31892 (lose i)))
		))
