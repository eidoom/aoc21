#!/usr/bin/env -S sbcl --script

(require "asdf")

; copied from d04.lisp
(defun split-on-subseq (str delim)
	(let ((a (search delim str)))
		(if (null a)
			(list str)
			(let ((b (length delim)))
				(cons (subseq str 0 a) (split-on-subseq (subseq str (+ a b)) delim))))))

(defun read-file (filename)
	(let ((data (uiop:read-file-lines filename)))
		(list
			(first data)
			(mapcar (lambda (x) (mapcar 'intern (split-on-subseq x " -> "))) (subseq data 2))
			)))

(defun pairwise (lst)
	(loop for i upto (- (length lst) 2)
				collect (intern (coerce (list (elt lst i) (elt lst (+ i 1))) 'string))
				))

; should use arrays instead of strings
(defun polymerise (data steps)
	(let ((after (first data))
				(rules (second data)))
		(dotimes (i steps)
			(let ((pairs (pairwise after)))
				(setf after (subseq after 0 1))
				(dolist (pair pairs)
					(let ((match (second (assoc pair rules))))
						(when (not (null match))
							(setf after (concatenate 'string after (string match)))
							)
						(setf after (concatenate 'string after (subseq (string pair) 1 2)))
						))))
		(let ((counts (mapcar (lambda (x) (count x after)) (remove-duplicates (coerce after 'list)))))
			(- (apply 'max counts) (apply 'min counts))
			)))

(time
	(let ((t0 (read-file "i14t0.txt"))
				(i (read-file "i14.txt"))
				)

		; (format t "~a~%" t0)
		(format t "~a~%" (= 1588 (polymerise t0 10)))
		(format t "~a~%" (= 3408 (polymerise i 10)))

		(print (polymerise t0 20))
		; (format t "~a~%" (= 2188189693529 (polymerise t0 40)))
		))
