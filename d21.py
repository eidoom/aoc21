#!/usr/bin/env pypy3

import collections
import copy


def parse(filename):
    with open(filename, "r") as f:
        return [int(x[28:]) for x in f.read().strip("\n").splitlines()]


def mod1(n, m):
    return (n - 1) % m + 1


def det_dice(starts, win=1e3):
    p = copy.deepcopy(starts)  # player positions on track
    s = [0, 0]  # player scores
    d = 0  # number of dice rolls
    t = 0  # whose turn it is (zero index)
    while True:
        r = 0  # dice triple roll result
        for _ in range(3):
            d += 1
            r += mod1(d, 100)
        p[t] = mod1(p[t] + r, 10)
        s[t] += p[t]
        if s[t] >= win:
            return d * s[(t + 1) % 2]
        t = (t + 1) % 2


def qu_dice(starts, rolls, win=21):
    games = [(starts, [0, 0], 0, 1)]
    winners = [0, 0]
    while len(games):
        p, s, t, c = games.pop()
        for r, rc in rolls.items():
            cn = c * rc
            pn = mod1(p[t] + r, 10)
            sn = s[t] + pn
            if sn >= win:
                winners[t] += cn
            else:
                if t == 0:
                    games.append(([pn, p[1]], [sn, s[1]], 1, cn))
                else:
                    games.append(([p[0], pn], [s[0], sn], 0, cn))
    return max(winners)


def quantum_rolls():
    histories = [(1, i) for i in range(1, 4)]
    final = collections.Counter()
    while len(histories):
        dep, tot = histories.pop()
        if dep == 3:
            final[tot] += 1
        else:
            for i in range(1, 4):
                histories += [(dep + 1, tot + i)]
    return final


if __name__ == "__main__":
    t0 = parse("i21t0.txt")
    data = parse("i21.txt")

    ta = det_dice(t0)
    print(ta, ta == 739785)
    ra = det_dice(data)
    print(ra, ra == 678468)

    rolls = quantum_rolls()

    tb = qu_dice(t0, rolls)
    print(tb, tb == 444356092776315)
    rb = qu_dice(data, rolls)
    print(rb, rb == 131180774190079)
