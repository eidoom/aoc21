#!/usr/bin/env python3

import functools


def parse(filename):
    with open(filename, "r") as f:
        return f.read().strip("\n").splitlines()


PAIRS = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">",
}


def autocomplete(line):
    to_match = []
    for i, char in enumerate(line):
        if char in PAIRS.keys():
            to_match.append(PAIRS[char])
        else:
            if to_match[-1] == char:
                to_match.pop()
            else:
                return False, char
    return True, "".join(reversed(to_match))


CHECKER_SCORES = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}


def syntax_error_score(nav):
    return sum(
        CHECKER_SCORES[res]
        for uncorrupt, res in map(autocomplete, nav)
        if not uncorrupt
    )


COMPLETER_SCORES = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4,
}


def score_completion(completion):
    return functools.reduce(
        lambda score, char: score * 5 + COMPLETER_SCORES[char], completion, 0
    )


def middle_score(nav):
    completions = [
        score_completion(completion)
        for uncorrupt, completion in map(autocomplete, nav)
        if uncorrupt
    ]
    return sorted(completions)[len(completions) // 2]


if __name__ == "__main__":
    test = parse("i10t0.txt")
    data = parse("i10.txt")

    print(a0 := autocomplete("{([(<{}[<>[]}>{[]{[(<()>")[1], a0 == "}")
    print(a1 := autocomplete("[[<[([]))<([[{}[[()]]]")[1], a1 == ")")
    print(a2 := autocomplete("[{[{({}]{}}([{[{{{}}([]")[1], a2 == "]")
    print(a3 := autocomplete("[<(<(<(<{}))><([]([]()")[1], a3 == ")")
    print(a4 := autocomplete("<{([([[(<>()){}]>(<<{{")[1], a4 == ">")

    print(ta := syntax_error_score(test), ta == 26397)
    print(ra := syntax_error_score(data), ra == 415953)

    print(b0 := autocomplete("[({(<(())[]>[[{[]{<()<>>")[1], b0 == "}}]])})]")
    print(b1 := autocomplete("[(()[<>])]({[<{<<[]>>(")[1], b1 == ")}>]})")
    print(b2 := autocomplete("(((({<>}<{<{<>}{[]{[]{}")[1], b2 == "}}>}>))))")
    print(b3 := autocomplete("{<[[]]>}<{[{[{[]{()[[[]")[1], b3 == "]]}}]}]}>")
    print(b4 := autocomplete("<{([{{}}[<[[[<>{}]]]>[]]")[1], b4 == "])}>")

    print(c0 := score_completion("])}>"), c0 == 294)
    print(c1 := score_completion("}}]])})]"), c1 == 288957)
    print(c2 := score_completion(")}>]})"), c2 == 5566)
    print(c3 := score_completion("]]}}]}]}>"), c3 == 995444)
    print(c4 := score_completion("])}>"), c4 == 294)

    print(tb := middle_score(test), tb == 288957)
    print(rb := middle_score(data), rb == 2292863731)
