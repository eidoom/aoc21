#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        return [
            (a, int(b))
            for a, b in [x.split(" ") for x in f.read().strip("\n").splitlines()]
        ]


def pilot(course):
    h = 0
    d = 0
    for c, v in course:
        if c == "forward":
            h += v
        elif c == "up":
            d -= v
        elif c == "down":
            d += v
    return h * d


def aiming(course):
    hpos = 0
    depth = 0
    aim = 0
    for c, v in course:
        if c == "forward":
            hpos += v
            depth += aim * v
        elif c == "up":
            aim -= v
        elif c == "down":
            aim += v
    return hpos * depth


if __name__ == "__main__":
    test = parse("i02t0.txt")
    data = parse("i02.txt")

    print(ta := pilot(test), ta == 150)
    print(ra := pilot(data), ra == 2039256)

    print(tb := aiming(test), tb == 900)
    print(rb := aiming(data), rb == 1856459736)
