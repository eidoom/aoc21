#!/usr/bin/env python3

from copy import deepcopy


def parse(filename):
    with open(filename, "r") as f:
        return [[c for c in row] for row in f.read().strip("\n").splitlines()]


# def show(im):
#     return "".join("".join(line) + "\n" for line in im)


def stationary_sea_cucumbers(old):
    w = len(old[0])
    h = len(old)
    new = deepcopy(old)
    c = 0
    while True:
        c += 1
        for i in range(h):
            for j in range(w):
                if old[i][j] == ">" and old[i][(j + 1) % w] == ".":
                    new[i][j] = "."
                    new[i][(j + 1) % w] = ">"
        mid = deepcopy(new)
        for i in range(h):
            for j in range(w):
                if mid[i][j] == "v" and mid[(i + 1) % h][j] == ".":
                    new[i][j] = "."
                    new[(i + 1) % h][j] = "v"
        if old == new:
            return c
        old = deepcopy(new)


if __name__ == "__main__":
    t0 = parse("i25t0.txt")
    data = parse("i25.txt")

    print(ta := stationary_sea_cucumbers(t0), ta == 58)
    print(ra := stationary_sea_cucumbers(data), ra == 456)
