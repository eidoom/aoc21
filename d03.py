#!/usr/bin/env python3

import operator


def parse(filename):
    with open(filename, "r") as f:
        return f.read().strip("\n").splitlines()


def power(report):
    m = len(report[0])
    n = len(report)
    gamma = 0
    epsilon = 0
    for i in range(m):
        j = m - i - 1
        if sum(1 for line in report if line[i] == "1") > n / 2:
            gamma += 2 ** j
        else:
            epsilon += 2 ** j
    return gamma * epsilon


def rating(report, cmp, i=0):
    n = len(report)

    if n == 1:
        return sum(2 ** i for i, v in enumerate(reversed(report[0])) if v == "1")

    v = "1" if cmp(sum(1 for line in report if line[i] == "1"), n / 2) else "0"

    return rating([line for line in report if line[i] == v], cmp, i + 1)


def life(report):
    return rating(report, operator.ge) * rating(report, operator.lt)


if __name__ == "__main__":
    test = parse("i03t0.txt")
    data = parse("i03.txt")

    print(ta := power(test), ta == 198)
    print(ra := power(data), ra == 1071734)

    print(tb := life(test), tb == 230)
    print(rb := life(data), rb == 6124992)
