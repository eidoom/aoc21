# [aoc21](https://gitlab.com/eidoom/aoc21)

* https://adventofcode.com/2021

## Python
* https://docs.python.org/3/
## Lisp
![](https://imgs.xkcd.com/comics/lisp.jpg)
* https://en.wikipedia.org/wiki/Lisp_(programming_language)
### Common Lisp
* https://lisp-lang.org/
* https://en.wikipedia.org/wiki/Common_Lisp
* [Specification](http://www.lispworks.com/documentation/HyperSpec/Front/Contents.htm)
* [An implementation](https://sbcl.org/)
* https://common-lisp.net/project/asdf/
* https://gigamonkeys.com/book/
* https://lispcookbook.github.io/cl-cookbook/
* https://learnxinyminutes.com/docs/common-lisp/
* interpreter
    ```shell
    sudo dnf install sbcl
    sbcl --script hello_world.lisp
    ```
* can also compile to `.fasl`
### Scheme
* https://groups.csail.mit.edu/mac/projects/scheme/
    * https://en.wikipedia.org/wiki/Scheme_(programming_language)
* [An implementation](https://www.gnu.org/software/guile/)
    * https://en.wikipedia.org/wiki/GNU_Guile
```shell
sudo dnf install guile
guile hello_world.scm
```
### Racket
* https://racket-lang.org/
* https://github.com/racket/racket
* https://en.wikipedia.org/wiki/Racket_(programming_language)
```shell
sudo dnf install racket
racket hello_world.rkt
```
### Clojure
* Hosted on Java/JVM
* https://clojure.org/
* https://github.com/clojure/clojure
* https://en.wikipedia.org/wiki/Clojure
* Other hosts:
    * https://clojurescript.org/ targets JavaScript
```shell
sudo dnf install clojure
clojure hello_world.clj
```
### Hy
* Hosted on Python
* https://github.com/hylang/hy
* https://en.wikipedia.org/wiki/Hy
* https://docs.hylang.org/en/master/
```shell
pip3 install --pre --user hy
hy hello_world.hy
```
### LFE
* Hosted on Erlang/BEAM
* https://lfe.io/
* https://github.com/lfe/lfe
* https://en.wikipedia.org/wiki/LFE_(programming_language)
```shell
sudo dnf install erlang-lfe
lfe hello_world.lfe
```
### Owl Lisp
* functional Scheme dialect
* https://haltp.org/posts/owl.html
* https://gitlab.com/owl-lisp/owl
```shell
sudo dnf install owl-lisp
ol hello_world.ol
```
### Fennel
* Hosted on Lua
* https://fennel-lang.org/
* https://github.com/bakpakin/Fennel
### Janet
* https://janet-lang.org/
* https://github.com/janet-lang/janet
* https://janet-lang.org/docs/index.html
* Installation: download [prebuilt binary](https://github.com/janet-lang/janet/releases/) and add to `PATH`
* [Vim syntax files](https://github.com/janet-lang/janet.vim)
