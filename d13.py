#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        dots, folds = [s.splitlines() for s in f.read().strip("\n").split("\n\n")]
    return [[int(i) for i in d.split(",")] for d in dots], [
        (a, int(n)) for a, n in [fold[11:].split("=") for fold in folds]
    ]


def draw(pattern):
    return "".join(
        "".join(row) + ("\n" if i != len(pattern) - 1 else "")
        for i, row in enumerate(pattern)
    )


def fold_up(pattern):
    return [pattern[i] for i in range(len(pattern) - 1, -1, -1)]


def fold_left(pattern):
    return [fold_up(row) for row in pattern]


def overlap(one, two):
    w = len(one[0])
    h = len(one)
    new = [["."] * w for _ in range(h)]
    for i in range(h):
        for j in range(w):
            if one[i][j] == "#" or two[i][j] == "#":
                new[i][j] = "#"
    return new


def count_dots(pattern):
    return sum(x == "#" for row in pattern for x in row)


def origami(dots, folds, early=False):
    w = max(j for j, i in dots) + 1
    h = max(i for j, i in dots) + 1
    pattern = [["."] * w for _ in range(h)]

    for j, i in dots:
        pattern[i][j] = "#"

    for axis, num in folds:
        if axis == "y":
            top = pattern[:num]
            bottom = pattern[num + 1 :]
            pattern = overlap(top, fold_up(bottom))
        elif axis == "x":
            left = [row[:num] for row in pattern]
            right = [row[num + 1 :] for row in pattern]
            pattern = overlap(left, fold_left(right))
        if early:
            return count_dots(pattern)

    return pattern


ALPHABET = {
    """\
.##.
#..#
#..#
####
#..#
#..#""": "A",
    """\
###.
#..#
###.
#..#
#..#
###.""": "B",
    """\
#..#
#.#.
##..
#.#.
#.#.
#..#""": "K",
    """\
..##
...#
...#
...#
#..#
.##.""": "J",
    """\
####
#...
###.
#...
#...
#...""": "F",
    """\
.##.
#..#
#...
#.##
#..#
.###""": "G",
    """\
.##.
#..#
#...
#...
#..#
.##.""": "C",
}


def read_letters(pattern):
    return "".join(
        ALPHABET[draw([row[i : i + 4] for row in pattern])] for i in range(0, 8 * 5, 5)
    )


if __name__ == "__main__":
    test = parse("i13t0.txt")
    data = parse("i13.txt")

    print(ta := origami(*test, True), ta == 17)
    print(ra := origami(*data, True), ra == 745)

    print(
        tb := draw(origami(*test)),
        tb == "#####\n#...#\n#...#\n#...#\n#####\n.....\n.....",
    )
    print(draw(rb := origami(*data)), rbl := read_letters(rb), rbl == "ABKJFBGC")
