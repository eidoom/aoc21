#!/usr/bin/env janet

(defn read-file [filename]
	(let [fishes (->> filename slurp string/trim (string/split ",") (map scan-number))]
		(seq [i :range [0 9]] (count |(= $ i) fishes))))

(defn growth [init days]
	(let [popn (array/slice init)]
		(for _ 0 days
			(let [tmp (popn 0)]
				(for i 0 8
					(put popn i (popn (inc i))))
				(+= (popn 6) tmp)
				(put popn 8 tmp)))
		(+ ;popn)))

(let [t0 (read-file "i06t0.txt")
			i (read-file "i06.txt")]

	(print (= 5934 (growth t0 80)))
	(print (= 380612 (growth i 80)))

	(print (= 26984457539 (growth t0 256)))
	(print (= 1710166656900 (growth i 256)))
	)
