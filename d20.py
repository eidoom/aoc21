#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        algo, im = f.read().strip("\n").split("\n\n")
    if len(algo) != 512:
        raise Exception("Image enhancement algorithm is not 512 characters long")
    return algo, [[c for c in row] for row in im.splitlines()]


def binim2dec(line):
    return sum([2 ** i for i, v in enumerate(reversed(line)) if v == "#"])


# def show(im):
#     return "".join("".join(line) + "\n" for line in im)


def enhance(algo, out, steps):
    pad = 2
    boundary = "."
    # print(show(out))
    for step in range(steps):
        w = len(out[0]) + pad * 2
        im = (
            [[boundary] * w] * pad
            + [[boundary] * pad + line + [boundary] * pad for line in out]
            + [[boundary] * w] * pad
        )
        h = len(im)
        if algo[0] == "#":
            boundary = algo[-1] if step % 2 else algo[0]
        out = [[boundary] * w for _ in range(h)]
        for i in range(1, h - 1):
            for j in range(1, w - 1):
                square = (
                    im[i - 1][j - 1 : j + 2]
                    + im[i][j - 1 : j + 2]
                    + im[i + 1][j - 1 : j + 2]
                )
                # print(show([square[i * 3 : i * 3 + 3] for i in range(3)]))
                out[i][j] = algo[binim2dec(square)]
        # print(show(out))
    return float("inf") if steps % 2 else sum(1 for row in out for c in row if c == "#")


if __name__ == "__main__":
    t0 = parse("i20t0.txt")
    data = parse("i20.txt")

    print(ta := enhance(*t0, 2), ta == 35)
    print(ra := enhance(*data, 2), ra == 5179)

    print(tb := enhance(*t0, 50), tb == 3351)
    print(rb := enhance(*data, 50), rb == 16112)
