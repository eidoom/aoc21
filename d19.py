#!/usr/bin/env python3

import itertools


def parse(filename):
    with open(filename, "r") as f:
        return [
            [tuple(map(int, beacon.split(","))) for beacon in scanner.splitlines()[1:]]
            for scanner in f.read().strip("\n").split("\n\n")
        ]


# manhattan distances
def fingerprint(scanner):
    return set(
        sum(abs(q0 - q1) for q0, q1 in zip(b0, b1))
        for b0, b1 in itertools.combinations(scanner, 2)
    )


# rotation matrices (3 axes)
"""
Rx(a)=
1   0   0
0   ca -sa
0   sa  ca

Ry(a)=
ca  0   sa
0   1   0
-sa 0   ca

Rz(a)=
ca  -sa 0
sa  sa  0
0   0   1
"""
# + reflections
# to reflection through plane `ax+by+cz=0` (normal vector (a,b,c))
"""
R(a,b,c)=
1-2a^2 -2ab   -2ac
-2ab   1-2b^2 -2bc
-2ac   -2bc   1-2c^2
"""
# we just need the ordinal subset of rotations (a=0,pi/2,pi,3pi/2) and reflections, size 48, e.g.
"""
R(1,0,0)=
-1 0  0
0  1  0
0  0  1

Rx(p/2)=
1  0  0
0  0  -1
0  1  0
"""
# etc
# except actually we just want the non-inverting ones (24)


def determinant(matrix):
    (a, b, c), (d, e, f), (g, h, i) = matrix
    return a * e * i + b * f * g + c * d * h - c * e * g - b * d * i - a * f * h


# keep only orientation-preserving transformations (det(M) > 0)
def rotations():
    for x in (-1, 1):
        for y in (-1, 1):
            for z in (-1, 1):
                for m in itertools.permutations(((x, 0, 0), (0, y, 0), (0, 0, z)), 3):
                    if determinant(m) == 1:
                        yield m


def transform(matrix, vector):
    (a, b, c), (d, e, f), (g, h, i) = matrix
    x, y, z = vector
    return ((a * x + b * y + c * z), (d * x + e * y + f * z), (g * x + h * y + i * z))


def align(scanner):
    for rot in rotations():
        new = []
        for beacon in scanner:
            new.append(transform(rot, beacon))
        yield new


def reorient(scanners):
    for s0, s1 in itertools.combinations(scanners, 2):
        for s2 in align(s1):
            eq = set(s0) == set(s2)
            if eq:
                print("Y")
                break


def identify(scanners):
    vertices = 12
    edges = vertices * (vertices - 1) // 2
    for s0, s1 in itertools.combinations(scanners, 2):
        ee = len(fingerprint(s0) & fingerprint(s1))
        if ee >= edges:
            for s2 in align(s1):
                prev = []
                for b0, b2 in itertools.product(s0, s2):
                    ds = [q1 - q0 for q0, q1 in zip(b0, b2)]
                    if ds in prev:
                        print(b0, ds)
                        break # break
                    prev.append(ds)
            exit()


if __name__ == "__main__":
    t0 = parse("i19t0.txt")
    t1 = parse("i19t1.txt")  # overlap
    t2 = parse("i19t2.txt")  # rotate
    # data = parse("i19.txt")

    # print(fingerprint(t2[0]))
    # print(overlap(t1))
    # print(reorient(t2))
    print(identify(t0))

    # print(ta := calc(t0), ta == 79)
    # print(ra := calc(data), ra == 380612)

    # print(tb := calc(t0), tb == 26984457539)
    # print(rb := calc(data), rb == 1710166656900)
