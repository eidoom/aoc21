#!/usr/bin/env python3

from operator import mul
from functools import reduce

PEAK = 9


def parse(filename):
    with open(filename, "r") as f:
        return [[int(i) for i in line] for line in f.read().strip("\n").splitlines()]


def up(hmap, i, j):
    return hmap[i - 1][j] if i > 0 else PEAK


def down(hmap, i, j):
    return hmap[i + 1][j] if i < len(hmap) - 1 else PEAK


def left(hmap, i, j):
    return hmap[i][j - 1] if j > 0 else PEAK


def right(hmap, i, j):
    return hmap[i][j + 1] if j < len(hmap[i]) - 1 else PEAK


def check_low_point(hmap, i, j):
    return all(
        hmap[i][j] < x for x in (func(hmap, i, j) for func in (up, down, left, right))
    )


def lowest_points(hmap):
    pts = []
    for i in range(len(hmap)):
        for j in range(len(hmap[i])):
            if check_low_point(hmap, i, j):
                pts.append((i, j))
    return pts


def lowest_points_score(hmap, lows):
    return sum(1 + hmap[i][j] for i, j in lows)


def size_basin(hmap, i, j, basin):
    if (i, j) not in basin and hmap[i][j] != PEAK:
        basin.add((i, j))
        if i > 0:
            basin |= size_basin(hmap, i - 1, j, basin)
        if i < len(hmap) - 1:
            basin |= size_basin(hmap, i + 1, j, basin)
        if j > 0:
            basin |= size_basin(hmap, i, j - 1, basin)
        if j < len(hmap[i]) - 1:
            basin |= size_basin(hmap, i, j + 1, basin)
    return basin


def largest_basins(hmap, seeds):
    return reduce(
        mul, sorted([len(size_basin(hmap, *seed, set())) for seed in seeds])[-3:]
    )


def one(filename, ra, rb, name):
    hmap = parse(filename)
    low = lowest_points(hmap)
    print(name, "1.", ta := lowest_points_score(hmap, low), ta == ra)
    print(name, "2.", tb := largest_basins(hmap, low), tb == rb)


if __name__ == "__main__":
    one("i09t0.txt", 15, 1134, "test")
    one("i09.txt", 572, 847044, "prod")
