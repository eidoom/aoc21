#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        return [int(x) for x in f.read().strip("\n").split(",")]


# def optim(crabs):
#     best = 1e9
#     for i in range(min(crabs), max(crabs) + 1):
#         fuel = 0
#         for crab in crabs:
#             fuel += abs(crab - i)
#         if fuel < best:
#             best = fuel
#     return best


def optim(crabs, func):
    popn = {x: crabs.count(x) for x in crabs}
    best = float("inf")
    for i in range(min(popn.keys()), max(popn.keys()) + 1):
        fuel = 0
        for pos, num in popn.items():
            fuel += num * func(pos, i)
        if fuel < best:
            best = fuel
    return best

def func1(pos, i):
    return abs(pos - i)

def optim1(crabs):
    return optim(crabs, func1)

def func2(pos, i):
    diff = func1(pos, i)
    return diff * (diff + 1) // 2

def optim2(crabs):
    return optim(crabs, func2)


if __name__ == "__main__":
    test = parse("i07t0.txt")
    data = parse("i07.txt")

    print(ta := optim1(test), ta == 37)
    print(rb := optim1(data), rb == 329389)

    print(ta := optim2(test), ta == 168)
    print(tb := optim2(data), tb == 86397080)
