SRCS=$(wildcard *.cpp)
PRGS=$(addsuffix _cpp, $(basename $(SRCS)))

all: $(PRGS)

%_cpp: %.cpp
	g++ -std=c++20 $< -o $@
