#!/usr/bin/env -S sbcl --script

(require "asdf")

(defun read-file (filename)
	(loop for line in (uiop:read-file-lines filename)
				for instruction = (uiop:split-string line)
				for direction = (first instruction)
				collect (list
									(cond
										( (string= direction "forward") 'forward)
										( (string= direction "up") 'up)
										( (string= direction "down") 'down) )
									(parse-integer (second instruction))
									)))

(defun pilot (course)
	(let ((h 0)
				(d 0))
		(loop for (dir len) in course
					do (case dir
							 ( 'forward (incf h len))
							 ( 'up (decf d len))
							 ( 'down (incf d len))))
		(* h d)
		))

(defun aiming (course)
	(let ((hpos 0)
				(depth 0)
				(aim 0))
		(loop for (dir len) in course
					do (case dir
							 ( 'forward (incf hpos len) (incf depth (* aim len)))
							 ( 'up (decf aim len))
							 ( 'down (incf aim len))))
		(* hpos depth)
		))

(time (let ((t0 (read-file "i02t0.txt"))
						(i (read-file "i02.txt")))

				(format t "~a~%" (= 150 (pilot t0)))
				(format t "~a~%" (= 2039256 (pilot i)))

				(format t "~a~%" (= 900 (aiming t0)))
				(format t "~a~%" (= 1856459736 (aiming i)))
				))
