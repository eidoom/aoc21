#!/usr/bin/env python3

import functools
import operator


def parse(filename):
    with open(filename, "r") as f:
        return f.readline().strip("\n")


def hex2bin(num_hex):
    return {
        "0": "0000",
        "1": "0001",
        "2": "0010",
        "3": "0011",
        "4": "0100",
        "5": "0101",
        "6": "0110",
        "7": "0111",
        "8": "1000",
        "9": "1001",
        "A": "1010",
        "B": "1011",
        "C": "1100",
        "D": "1101",
        "E": "1110",
        "F": "1111",
    }[num_hex]


def bin2dec(num_bin):
    num_dec = 0
    for i, c in enumerate(reversed(num_bin)):
        if c == "1":
            num_dec += 2 ** i
    return num_dec


def packet(pac):
    version_sum = bin2dec(pac[:3])
    type_id = bin2dec(pac[3:6])

    # literal value
    if type_id == 4:
        i = 6
        bin_num = ""
        while True:
            bin_num += pac[i + 1 : i + 5]

            if pac[i] == "0":
                break

            i += 5
        return i + 5, bin2dec(bin_num), version_sum

    # operator packet
    else:
        subpackets = []

        if pac[6] == "0":
            sublength = bin2dec(pac[7:22])  # length in bits of sub-packets
            e = 22 + sublength
            s = 0
            while s < sublength:
                j, lit, vv = packet(pac[22 + s : e])
                subpackets.append(lit)
                s += j
                version_sum += vv
            s += 22

        else:
            s = 18
            num_pacs = bin2dec(pac[7:s])  # number of immediate sub-packets
            for _ in range(num_pacs):
                j, lit, vv = packet(pac[s:])
                subpackets.append(lit)
                s += j
                version_sum += vv

        match type_id:
            case 0:
                value = sum(subpackets)
            case 1:
                value = functools.reduce(operator.mul, subpackets)
            case 2:
                value = min(subpackets)
            case 3:
                value = max(subpackets)
            case 5:
                value = int(subpackets[0] > subpackets[1])
            case 6:
                value = int(subpackets[0] < subpackets[1])
            case 7:
                value = int(subpackets[0] == subpackets[1])

        return s, value, version_sum


def decode(mes_hex):
    mes_bin = "".join(map(hex2bin, mes_hex))
    return packet(mes_bin)


if __name__ == "__main__":
    t0 = parse("i16t0.txt")
    t1 = parse("i16t1.txt")
    t2 = parse("i16t2.txt")
    t3 = parse("i16t3.txt")
    t4 = parse("i16t4.txt")
    t5 = parse("i16t5.txt")
    t6 = parse("i16t6.txt")
    t7 = parse("i16t7.txt")
    t8 =  parse("i16t8.txt")
    t9 =  parse("i16t9.txt")
    t10 = parse("i16t10.txt")
    t11 = parse("i16t11.txt")
    t12 = parse("i16t12.txt")
    t13 = parse("i16t13.txt")
    t14 = parse("i16t14.txt")
    data = parse("i16.txt")

    print(ta0 := decode(t0)[2], ta0 == 6)
    print(ta1 := decode(t1)[2], ta1 == 9)
    print(ta2 := decode(t2)[2], ta2 == 14)
    print(ta3 := decode(t3)[2], ta3 == 16)
    print(ta4 := decode(t4)[2], ta4 == 12)
    print(ta5 := decode(t5)[2], ta5 == 23)
    print(ta6 := decode(t6)[2], ta6 == 31)
    print(ra := decode(data)[2], ra == 929)

    print(tb7 :=  decode(t7)[1], tb7 == 3)
    print(tb8 :=  decode(t8)[1], tb8 == 54)
    print(tb9 :=  decode(t9)[1], tb9 == 7)
    print(tb10 := decode(t10)[1], tb10 == 9)
    print(tb11 := decode(t11)[1], tb11 == 1)
    print(tb12 := decode(t12)[1], tb12 == 0)
    print(tb13 := decode(t13)[1], tb13 == 0)
    print(tb14 := decode(t14)[1], tb14 == 1)
    print(rb := decode(data)[1], rb == 911945136934)
