#!/usr/bin/env python3

import heapq


def parse(filename):
    with open(filename, "r") as f:
        return [[int(i) for i in row] for row in f.read().strip("\n").splitlines()]


# priority queue built on min-binary-heap
# https://docs.python.org/3/library/heapq.html
# https://en.wikipedia.org/wiki/Priority_queue
# https://en.wikipedia.org/wiki/Heap_(data_structure)
# https://en.wikipedia.org/wiki/Binary_heap
# TODO use Fibonacci heap instead https://en.wikipedia.org/wiki/Fibonacci_heap
class PriorityQueue:
    def __init__(self, lst):
        self.heap = lst

    # add an element to the queue with an associated priority
    def add(self, priority, value):
        heapq.heappush(self.heap, (priority, value))

    # remove the element from the queue that has the lowest priority, and return it
    # note that this element is always at the front
    def pop(self):
        return heapq.heappop(self.heap)[1]


def neighbours(i, j, w, h):
    if i < h - 1:
        yield (i + 1, j)
    if i > 0:
        yield (i - 1, j)
    if j < w - 1:
        yield (i, j + 1)
    if j > 0:
        yield (i, j - 1)


# https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
def dijkstra(grid, i0, j0, i1, j1):
    w = len(grid[0])
    h = len(grid)

    dist = [[float("inf")] * w for _ in range(h)]
    dist[i0][j0] = 0

    queue = PriorityQueue([(0, (i0, j0))])

    while True:
        i, j = queue.pop()

        for a, b in neighbours(i, j, w, h):
            new = dist[i][j] + grid[a][b]

            if a == i1 and b == j1:
                return new

            if new < dist[a][b]:
                dist[a][b] = new
                queue.add(new, (a, b))


def extend(tile, mul=5):
    w = len(tile[0])
    h = len(tile)
    cave = [[None] * mul * w for _ in range(mul * h)]

    for y in range(mul):
        for x in range(mul):
            for i in range(h):
                for j in range(w):
                    cave[y * h + i][x * w + j] = ((tile[i][j] + x + y - 1) % 9) + 1

    return cave


def find_lowest_risk_path(grid):
    return dijkstra(grid, 0, 0, len(grid) - 1, len(grid[0]) - 1)


if __name__ == "__main__":
    test = parse("i15t0.txt")
    data = parse("i15.txt")

    print(ta := find_lowest_risk_path(test), ta == 40)
    print(ra := find_lowest_risk_path(data), ra == 527)

    test1 = parse("i15t1.txt")
    print(extend([[8]]) == test1)

    test2 = parse("i15t2.txt")
    new_test = extend(test)
    print(new_test == test2)

    print(tb := find_lowest_risk_path(new_test), tb == 315)
    print(rb := find_lowest_risk_path(extend(data)), rb == 2887)
