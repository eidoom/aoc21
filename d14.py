#!/usr/bin/env python3

from collections import Counter
from itertools import pairwise


def parse(filename):
    with open(filename, "r") as f:
        template, pairs = f.read().strip("\n").split("\n\n")
    return template, dict(pair.split(" -> ") for pair in pairs.splitlines())


def polymerise(template, rules, steps):
    # the number of each pair in the polymer
    # template = "NNCB" -> pairs = {"NN":1,"NC":1,"CB":1}
    pairs = Counter(["".join(x) for x in pairwise(template)])
    for _ in range(steps):
        # a new pairs Counter for the polymer of the next step
        new = Counter()
        for pair, count in pairs.items():
            try:
                # type(new) = dict
                # type(key) = str
                # eg pair = "AC", rule = {"AC": "B"}, so all AC pairs are lost and AB and CB pairs are incremented
                new[pair[0] + rules[pair]] += count
                new[rules[pair] + pair[1]] += count
            except KeyError:
                # if the dict key `pair` doesn't exist in `rules`, carry over the pair count (eg {"AC": 1}) to the new polymer
                new[pair] += count
        pairs = new

    # count the individual letters from the pairs
    # the first and last letters are not included in the pairs, so add them at the start
    letters = Counter([template[0], template[-1]])
    # decompose {"AB": 1} as (a="A",b="B",v=1)
    for (a, b), v in pairs.items():
        letters[a] += v
        letters[b] += v
    letters = letters.values()

    # since pairs overlap, we double counted
    return max(letters) // 2 - min(letters) // 2


if __name__ == "__main__":
    test = parse("i14t0.txt")
    data = parse("i14.txt")

    print(ta := polymerise(*test, 10), ta == 1588)
    print(ra := polymerise(*data, 10), ra == 3408)

    print(tb := polymerise(*test, 40), tb == 2188189693529)
    print(rb := polymerise(*data, 40), rb == 3724343376942)
