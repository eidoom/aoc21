#!/usr/bin/env python3

import string, heapq, copy


def parse(filename):
    with open(filename, "r") as f:
        rooms = [
            [line[i] for i in range(3, 10, 2)]
            for line in f.read().strip("\n").splitlines()[2:4]
        ]
    out = [".", "."]
    for a in zip(*reversed(rooms)):
        out.append(list(a))
        out.append(".")
    out.append(".")
    return out


ENERGY = {"A": 1, "B": 10, "C": 100, "D": 1000}
AMPHIPODS = string.ascii_uppercase[:4]


def show(state):
    out = (
        "#" * 13
        + "\n#"
        + "".join(state[:2])
        + "".join("." + state[i] for i in range(3, 8, 2))
        + "."
        + "".join(state[-2:])
        + "#\n"
        + "#" * 3
    )
    for i in range(2, 9, 2):
        try:
            out += state[i][1] + "#"
        except IndexError:
            out += ".#"
    out += "#" * 2 + "\n" + " " * 2 + "#"
    for i in range(2, 9, 2):
        try:
            out += state[i][0] + "#"
        except IndexError:
            out += ".#"
    out += " " * 2 + "\n" + " " * 2 + "#" * 9 + " " * 2
    return out


# see d15
class PriorityQueue:
    def __init__(self, lst):
        self.heap = lst

    def add(self, priority, value):
        heapq.heappush(self.heap, (priority, value))

    def pop(self):
        return heapq.heappop(self.heap)


def dijkstra(state):
    w = len(state)
    queue = PriorityQueue([(0, state)])

    while len(queue.heap):
        cost, state = queue.pop()

        # print(show(state))
        # input()

        if all(state[i] == [c, c] for i, c in zip(range(2, 9, 2), AMPHIPODS)):
            return cost

        for i, ss in enumerate(state):
            s = copy.copy(ss)
            if isinstance(s, list):
                if len(s):
                    a = s.pop()
                    for j in range(i):
                        if state[j] == "." and all(
                            state[k] == "."
                            for k in range(j + 1, i)
                            if isinstance(state[k], str)
                        ):
                            new_state = copy.deepcopy(state)
                            new_state[i] = s
                            new_state[j] = a
                            queue.add(
                                cost + ENERGY[a] * (i - j + (2 - len(s))), new_state
                            )
                    for j in range(i + 1, w):
                        if state[j] == "." and all(
                            state[k] == "."
                            for k in range(i + 1, w)
                            if isinstance(state[k], str)
                        ):
                            new_state = copy.deepcopy(state)
                            new_state[i] = s
                            new_state[j] = a
                            queue.add(
                                cost + ENERGY[a] * (j - i + (2 - len(s))), new_state
                            )
            else:
                for j, a in zip(range(2, 9, 2), AMPHIPODS):
                    if s == a and state[j] in ([], [a]):
                        new_state = copy.deepcopy(state)
                        new_state[i] = "."
                        new_state[j].append(a)
                        queue.add(
                            cost + ENERGY[a] * (abs(i - 2) + (2 - len(s))), new_state
                        )


if __name__ == "__main__":
    t0 = parse("i23t0.txt")
    # data = parse("i23.txt")

    # print(show(t0))
    print(dijkstra(t0))

    # print(ta := calc(t0), ta == 79)
    # print(ra := calc(data), ra == 380612)

    # print(tb := calc(t0), tb == 26984457539)
    # print(rb := calc(data), rb == 1710166656900)
