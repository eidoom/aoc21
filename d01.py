#!/usr/bin/env python3


def parse(filename):
    with open(filename, "r") as f:
        return [int(x) for x in f.read().strip("\n").splitlines()]


def largers(depths, s):
    return sum(a < b for a, b in zip(depths, depths[s:]))


if __name__ == "__main__":
    test = parse("i01t0.txt")
    data = parse("i01.txt")

    print(ta := largers(test, 1), ta == 7)
    print(ra := largers(data, 1), ra == 1393)

    print(tb := largers(test, 3), tb == 5)
    print(rb := largers(data, 3), rb == 1359)
