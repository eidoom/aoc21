#!/usr/bin/env -S sbcl --script

(require "asdf")

(defun read-file (filename)
	(mapcar 'parse-integer (uiop:read-file-lines filename)))

(defun largers (lst s)
	(count t (mapcar '< lst (nthcdr s lst)))
	)

; (defun largers-imperative (lst s)
; 	(let ((cnt 0))
; 		(dotimes (i (- (length lst) s))
; 			(if (< (elt lst i) (elt lst (+ i s)))
; 				(incf cnt))
; 			)
; 		cnt)
; 	)

(time
	(let ((t0 (read-file "i01t0.txt"))
				(i (read-file "i01.txt")))

		(format t "~a~%" (= 7 (largers t0 1)))
		(format t "~a~%" (= 1393 (largers i 1)))

		(format t "~a~%" (= 5 (largers t0 3)))
		(format t "~a~%" (= 1359 (largers i 3)))
		))
