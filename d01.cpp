#include <fstream>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

std::vector<int> parse(const std::string& filename)
{
	std::ifstream infile(filename);
	std::string line;
	std::vector<int> v;
	while (std::getline(infile, line)) {
		v.push_back(std::stoi(line));
	}
	return v;
}

int largers(const std::vector<int>& depths, int s)
{
	return std::transform_reduce(depths.begin(), depths.end() - s, depths.begin() + s, 0, std::plus<>(), std::less<>());
}

void result(int num, int exp)
{
	std::cout << (num == exp) << " " << num << '\n';
}

void part(const std::vector<int>& t, const std::vector<int>& d, int s, int te, int de)
{
	int t0 { largers(t, s) };
	int d0 { largers(d, s) };
	result(t0, te);
	result(d0, de);
}

int main()
{
	std::vector<int> t { parse("i01t0.txt") };
	std::vector<int> d { parse("i01.txt") };

	part(t, d, 1, 7, 1393);
	part(t, d, 3, 5, 1359);
}
