#!/usr/bin/env -S sbcl --script

(require "asdf")

(defun read-file (filename)
	(uiop:read-file-lines filename)
	)

(defun pow2 (pow)
	(ash 1 pow))

(defun power (report)
	(let ((m (length (first report)))
				(n (length report))
				(gamma 0)
				(epsilon 0))
		(dotimes (i m)
			(let ((j (- m (+ 1 i))))
				(if (> (count #\1 (mapcar (lambda (x) (char x i)) report)) (/ n 2))
					(incf gamma (pow2 j))
					(incf epsilon (pow2 j))
					))
			)
		(* gamma epsilon)
		)
	)

(defun bin2dec (bin)
	(let ((dec 0))
		(loop for x across bin
					and j downfrom (- (length bin) 1)
					do (when (char= #\1 x) (incf dec (pow2 j))))
		dec
		))

(defun rating (report i cmp)
	(let ((n (length report)) (v nil))
		(if (= 1 n) (return-from rating (bin2dec (first report))))
		(if (funcall cmp (count-if (lambda (x) (char= #\1 (char x i))) report) (/ n 2))
			(setf v #\1)
			(setf v #\0)
			)
		(rating (remove-if (lambda (x) (char/= v (char x i))) report) (+ i 1) cmp)
		))

(defun o2 (report)
	(rating report 0 '>=)
	)

(defun co2 (report)
	(rating report 0 '<)
	)

(defun life (report)
	(* (o2 report) (co2 report))
	)

(time
	(let ((t0 (read-file "i03t0.txt"))
				(i (read-file "i03.txt")))

		(format t "~a~%" (= 198 (power t0)))
		(format t "~a~%" (= 1071734 (power i)))

		(format t "~a~%" (= 230 (life t0)))
		(format t "~a~%" (= 6124992 (life i)))
		))
