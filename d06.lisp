#!/usr/bin/env -S sbcl --script

(require "asdf")

(defun read-file (filename)
	(let ((fishes (mapcar 'parse-integer (uiop:split-string (uiop:read-file-line filename) :separator ","))))
		(loop :for x :to 8 :collect (count x fishes))))

(defun growth (popn_ days)
	(let ((popn (copy-list popn_)))
		(dotimes (_ days)
			(let ((tmp (first popn)))
				(dotimes (i 8)
					(setf (elt popn i) (elt popn (+ 1 i))))
				(incf (elt popn 6) tmp)
				(setf (elt popn 8) tmp)
				))
		(apply #'+ popn)))

(time
	(let ((t0 (read-file "i06t0.txt"))
				(i (read-file "i06.txt")))

		(format t "~a~%" (= 5934 (growth t0 80)))
		(format t "~a~%" (= 380612 (growth i 80)))

		(format t "~a~%" (= 26984457539 (growth t0 256)))
		(format t "~a~%" (= 1710166656900 (growth i 256)))
		))
